const getSum = (str1, str2) => {
    if (typeof str1 === 'string' || str1 instanceof String)
        console.log("string1")
    else
        return false
    if (typeof str2 === 'string' || str2 instanceof String)
        console.log("string2")
    else
        return false


    if (isNaN(str1)) {
        return false
    }
    if (isNaN(str2)) {
        return false
    }

    // Before proceeding further, make
    // sure length of str2 is larger.
    if (str1.length > str2.length) {
        let t = str1;
        str1 = str2;
        str2 = t;
    }

    // Take an empty String for storing result
    let str = "";

    // Calculate length of both String
    let n1 = str1.length,
        n2 = str2.length;

    // Reverse both of Strings
    str1 = str1.split("").reverse().join("");
    str2 = str2.split("").reverse().join("");

    let carry = 0;
    for (let i = 0; i < n1; i++) {

        // Do school mathematics, compute sum of
        // current digits and carry
        let sum = ((str1[i].charCodeAt(0) -
                '0'.charCodeAt(0)) +
            (str2[i].charCodeAt(0) -
                '0'.charCodeAt(0)) + carry);
        str += String.fromCharCode(sum % 10 +
            '0'.charCodeAt(0));

        // Calculate carry for next step
        carry = Math.floor(sum / 10);
    }

    // Add remaining digits of larger number
    for (let i = n1; i < n2; i++) {
        let sum = ((str2[i].charCodeAt(0) -
            '0'.charCodeAt(0)) + carry);
        str += String.fromCharCode(sum % 10 +
            '0'.charCodeAt(0));
        carry = Math.floor(sum / 10);
    }

    // Add remaining carry
    if (carry > 0)
        str += String.fromCharCode(carry +
            '0'.charCodeAt(0));

    // reverse resultant String
    str = str.split("").reverse().join("");

    return str;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    var posts = 0;
    var comments = 0;
    for (var i = 0; i < listOfPosts.length; i++) {
        if (listOfPosts[i].author == authorName) {
            posts++;
        }
    }

    for (i = 0; i < listOfPosts.length; i++) {
        if (listOfPosts[i].hasOwnProperty("comments")) {
            for (var j = 0; j < listOfPosts[i].comments.length; j++) {
                if (listOfPosts[i].comments[j].author == authorName) {
                    comments++;
                }
            }
        }
    }
    return `Post:${posts},comments:${comments}`
}

const tickets = (people) => {
    let a = 0;
    let b = 0;
    let result = "YES";

    for (let val of people) {
        if (result == "NO") break;

        if (val == 25) {
            a++;
        }
        if (val == 50) {
            a--;
            b++;
            if (a < 0) result = "NO";
        }
        if (val == 100) {
            if (b) {
                b--;
                a--;
            } else {
                a -= 3;
            }
            if (a < 0 || b < 0) result = "NO";
        }
    }
    return result;
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };